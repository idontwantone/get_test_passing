class User < ApplicationRecord
  def last_name=(value)
    self[:last_name] = value.capitalize
  end

  def email=(value)
    @email = value.downcase
  end
end
