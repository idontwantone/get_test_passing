require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'full_name' do
    let(:user) { create(:user) }

    it 'returns first_name space last_name' do
      expect(user.full_name).to eq('John Doe')
    end
  end

  describe 'first_name=' do
    it 'sets capitalized first_name' do
      user = User.new
      user.first_name = "donnie"
      expect(user.first_name).to eq('Donnie')
    end
  end

  describe 'last_name=' do
    it 'sets capitalized last_name' do
      user = User.new
      user.last_name = "joHnson"
      expect(user.last_name).to eq('Johnson')
    end
  end

  describe 'email=' do
    it 'lowercases email' do
      user = User.new
      user.email = "RAD@GMAIL.com"
      expect(user.email).to eq('rad@gmail.com')
    end
  end
end
