require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  render_views

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    context "John user" do
      before do
        create(:user)
      end

      it "returns http success" do
        get :show, params: { id: 1 }
        expect(response).to have_http_status(:server_error)
      end

      it "shows user information" do
        get :show, params: { id: 1 }
        expect(response.body).to include("John")
        expect(response.body).to include("Doe")
      end
    end

    context "Bob user" do
      before do
        create(:user, first_name: 'Bob', last_name: 'Smith')
      end

      it "returns http success" do
        get :show, params: { id: 1 }
        expect(response).to have_http_status(:success)
      end

      it "shows user information" do
        get :show, params: { id: 1 }
        expect(response.body).to include("Bob")
        expect(response.body).to include("Smith")
      end
    end
  end
end
